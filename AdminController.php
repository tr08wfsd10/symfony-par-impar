<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdminController
{
	/**
 	 * 	@Route("/freya/{nums}", methods={"GET", "POST"})
 	 */
	public function freya($nums)
	{
		if(is_numeric($nums)){
			// return new Response ("El resultado es $nums");
			if (($nums%2)==0){
				return new Response ("$nums es par");
			}else{
				return new Response ("$nums es impar");
			}
		}else{
			if(is_string($nums)){
				for ($i=0; $i < strlen($nums); $i=$i+2) { 
					$nums[$i]=strtoupper($nums[$i]);
				}
			return new Response ($nums);
			}
		}
	}

}